use actix_web::{
    post,
    web::{Data, Json},
    HttpResponse, Responder,
};
use fishlist_base::fisher_auth::{LoginRequest, LogoutRequest, RegisterRequest};
use fishlist_data::types::DbConn;

mod internal;

#[post("/auth/fisher/login")]
pub async fn login(conn: Data<DbConn>, login_request: Json<LoginRequest>) -> impl Responder {
    match internal::login_fisher(&conn, &login_request).await {
        Err(e) => HttpResponse::new(e),
        Ok(v) => HttpResponse::Ok().json(v),
    }
}

#[post("/auth/fisher/register")]
pub async fn register(
    conn: Data<DbConn>,
    register_request: Json<RegisterRequest>,
) -> impl Responder {
    HttpResponse::new(internal::register_fisher(&conn, &register_request).await)
}

#[post("/auth/fisher/logout")]
pub async fn logout(conn: Data<DbConn>, logout_request: Json<LogoutRequest>) -> impl Responder {
    HttpResponse::new(internal::logout_fisher(&conn, &logout_request).await)
}
