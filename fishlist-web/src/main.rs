use actix_web::{middleware::Logger, App, HttpServer};
use fishlist_data::init_data_pool;
use std::io;

mod events_resource;
mod fish_events_resource;
mod fisher_auth_resource;
pub mod util;
mod wishes_resource;

#[actix_web::main]
async fn main() -> io::Result<()> {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();
    let pool = init_data_pool().await?;
    // Start HTTP server
    HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .service(fisher_auth_resource::register)
            .service(fisher_auth_resource::login)
            .service(fisher_auth_resource::logout)
            .service(events_resource::create_event)
            .service(events_resource::get_events)
            .service(events_resource::edit_event_details)
            .service(events_resource::delete_event)
            .service(wishes_resource::add_wish)
            .service(wishes_resource::get_wishes)
            .service(wishes_resource::update_wish)
            .service(wishes_resource::delete_wish)
            .service(fish_events_resource::add_fish_to_event)
            .wrap(Logger::default())
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
