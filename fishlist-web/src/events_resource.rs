use actix_web::{
    delete, get, post, put,
    web::{Data, Json},
    HttpResponse, Responder,
};
use fishlist_base::events::{
    CreateEventRequest, DeleteEventRequest, GetEventsRequest, UpdateEventRequest,
};
use fishlist_data::types::DbConn;

mod internal;

#[get("/get/events")]
pub async fn get_events(
    conn: Data<DbConn>,
    get_events_req: Json<GetEventsRequest>,
) -> impl Responder {
    match internal::get_events(&conn, &get_events_req).await {
        Err(e) => HttpResponse::new(e),
        Ok(v) => HttpResponse::Ok().json(v),
    }
}

#[post("/create/event")]
pub async fn create_event(
    conn: Data<DbConn>,
    create_event: Json<CreateEventRequest>,
) -> impl Responder {
    match internal::create_event(&conn, &create_event).await {
        Err(e) => HttpResponse::new(e),
        Ok(v) => HttpResponse::Ok().json(v),
    }
}

#[put("/edit/event/details")]
pub async fn edit_event_details(
    conn: Data<DbConn>,
    update_event_req: Json<UpdateEventRequest>,
) -> impl Responder {
    HttpResponse::new(internal::update_event_details(&conn, &update_event_req).await)
}

#[delete("/delete/event")]
pub async fn delete_event(
    conn: Data<DbConn>,
    delete_event_req: Json<DeleteEventRequest>,
) -> impl Responder {
    HttpResponse::new(internal::delete_event_and_wishes(&conn, &delete_event_req).await)
}
