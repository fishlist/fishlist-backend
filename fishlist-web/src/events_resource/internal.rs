use actix_web::http::StatusCode;
use fishlist_base::events::{
    CreateEventRequest, CreateEventResponse, DeleteEventRequest, GetEventsRequest,
    GetEventsResponse, UpdateEventRequest,
};
use fishlist_core::events::{event_valid, SimpleEvent};
use fishlist_data::{
    events::{
        create_new_event, delete_event, event_exists_for_fisher, get_event_data, update_event,
    },
    types::DbConn,
};
use futures_util::future::TryFutureExt;
use util::{easy_fatal_error, valid_fisher_session};

use crate::util;

pub async fn get_events(
    conn: &DbConn,
    get_events_req: &GetEventsRequest,
) -> Result<Vec<GetEventsResponse>, StatusCode> {
    let future = valid_fisher_session(conn, &get_events_req.session_token);

    future
        .and_then(move |fisher_id| async move {
            get_event_data(conn, fisher_id)
                .map_err(|e| easy_fatal_error("getting event data", e))
                .await
        })
        .await
}

pub async fn create_event(
    conn: &DbConn,
    create_event: &CreateEventRequest,
) -> Result<CreateEventResponse, StatusCode> {
    let future = event_valid(SimpleEvent::from(create_event))
        .map_err(|code| StatusCode::from_u16(code).unwrap());

    match future
        .and_then(
            move |_| async move { valid_fisher_session(conn, &create_event.session_token).await },
        )
        .and_then(move |fisher_id| async move {
            create_new_event(conn, create_event, fisher_id)
                .await
                .map_err(|e| easy_fatal_error("creating new event", e))
        })
        .and_then(move |event_id| async move { Ok(CreateEventResponse { event_id }) })
        .await
    {
        Ok(v) => Ok(v),
        Err(e) => Err(e),
    }
}

pub async fn update_event_details(
    conn: &DbConn,
    update_event_req: &UpdateEventRequest,
) -> StatusCode {
    let future = event_valid(SimpleEvent::from(update_event_req))
        .map_err(|code| StatusCode::from_u16(code).unwrap());

    match future
        .and_then(move |_| async move {
            valid_fisher_session(conn, &update_event_req.session_token).await
        })
        .and_then(move |fisher_id| async move {
            event_exists_for_fisher(conn, update_event_req.event_id, fisher_id)
                .await
                .map_err(|e| easy_fatal_error("checking if event exists", e))
                .map(|exists| (exists, fisher_id))
        })
        .and_then(move |(exists, fisher_id)| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                update_event(conn, update_event_req, fisher_id)
                    .await
                    .map_err(|e| easy_fatal_error("updating event", e))
            }
        })
        .await
    {
        Ok(_) => StatusCode::from_u16(204).unwrap(),
        Err(e) => e,
    }
}

pub async fn delete_event_and_wishes(
    conn: &DbConn,
    delete_event_req: &DeleteEventRequest,
) -> StatusCode {
    let future = valid_fisher_session(conn, &delete_event_req.session_token);

    match future
        .and_then(move |fisher_id| async move {
            delete_event(conn, delete_event_req, fisher_id)
                .map_err(|e| easy_fatal_error("deleting event", e))
                .await
        })
        .await
    {
        Ok(_) => StatusCode::from_u16(204).unwrap(),
        Err(e) => e,
    }
}
