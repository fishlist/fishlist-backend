use actix_web::{
    post,
    web::{Data, Json},
    HttpResponse, Responder,
};
use fishlist_base::fish_events::AddFishToEventRequest;
use fishlist_data::types::DbConn;

mod internal;

#[post("/add/event/fish")]
pub async fn add_fish_to_event(
    conn: Data<DbConn>,
    add_to_event_req: Json<AddFishToEventRequest>,
) -> impl Responder {
    match internal::add_fish_to_event(&conn, &add_to_event_req).await {
        Ok(v) => HttpResponse::Ok().json(v),
        Err(e) => HttpResponse::new(e),
    }
}
