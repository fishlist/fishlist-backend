use actix_web::http::StatusCode;
use fishlist_base::fisher_auth::{LoginRequest, LoginResponse, LogoutRequest, RegisterRequest};
use fishlist_core::sec::verify_pw;
use fishlist_data::{
    fisher_auth::{add_new_fisher, delete_session_token, get_hashword, setup_new_session},
    fisher_common::{fisher_email_exists, fisher_exists, get_id_from_email},
    types::{DbConn, SqlError},
};
use futures_util::future::TryFutureExt;
use log::error;
use util::easy_fatal_error;

use crate::util;

pub async fn logout_fisher(conn: &DbConn, logout_request: &LogoutRequest) -> StatusCode {
    match delete_session_token(conn, &logout_request.token).await {
        Ok(_) => StatusCode::from_u16(204).unwrap(),
        Err(e) => easy_fatal_error("logging user out", e),
    }
}

pub async fn login_fisher(
    conn: &DbConn,
    login_request: &LoginRequest,
) -> Result<LoginResponse, StatusCode> {
    let future = fisher_email_exists(conn, &login_request.email);
    future
        .map_err(|e| easy_fatal_error("checking if email exists", e))
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                user_password_correct(conn, login_request)
                    .await
                    .map_err(|e| easy_fatal_error("checking if password is correct", e))
            }
        })
        .and_then(move |pw_correct| async move {
            if !pw_correct {
                Err(StatusCode::from_u16(401).unwrap())
            } else {
                get_id_from_email(conn, &login_request.email)
                    .await
                    .map_err(|e| easy_fatal_error("getting id from email", e))
            }
        })
        .and_then(move |id| async move {
            setup_new_session(conn, id)
                .await
                .map_err(|e| easy_fatal_error("setting up new token for user", e))
        })
        .await
}

pub async fn register_fisher(conn: &DbConn, register_request: &RegisterRequest) -> StatusCode {
    StatusCode::from_u16(
        match fisher_exists(
            &conn,
            &register_request.email,
            &register_request.display_name,
        )
        .await
        {
            Err(e) => {
                error!("Error checking if fisher exists: {}", e);
                500
            }
            Ok(v) => {
                if v {
                    409
                } else {
                    match add_new_fisher(&conn, &register_request).await {
                        Ok(_) => 204,
                        Err(e) => {
                            error!("Error registering fisher: {}", e);
                            500
                        }
                    }
                }
            }
        },
    )
    .unwrap()
}

async fn user_password_correct(
    conn: &DbConn,
    login_request: &LoginRequest,
) -> Result<bool, SqlError> {
    let raw_pw = &login_request.password;
    let hashword = get_hashword(conn, login_request).await?;

    Ok(verify_pw(raw_pw, &hashword).map_err(|e| {
        error!("Failed to hash password: {}", e);
        SqlError::WorkerCrashed
    })?)
}
