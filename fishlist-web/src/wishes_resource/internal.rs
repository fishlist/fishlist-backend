use actix_web::http::StatusCode;
use fishlist_base::wishes::{
    ClaimWishRequest, CreateWishRequest, CreateWishResponse, DeleteWishRequest, GetWishesRequest,
    GetWishesResponse, UpdateWishRequest,
};
use fishlist_core::wishes::{valid_wish, SimpleWish};
use fishlist_data::{
    events::{event_exists_for_fish, event_exists_for_fisher},
    types::DbConn,
    wishes::{
        add_new_wish, check_wish_can_be_claimed, delete_wish_data, get_wishes_for_event,
        set_wish_claimed, update_wish_details, wish_exists,
    },
};
use futures_util::future::TryFutureExt;
use util::{easy_fatal_error, valid_fish_session, valid_fisher_session};

use crate::util;

pub async fn add_wish(
    conn: &DbConn,
    create_wish_req: &CreateWishRequest,
) -> Result<CreateWishResponse, StatusCode> {
    let future = valid_wish(SimpleWish::from(create_wish_req))
        .map_err(|status| StatusCode::from_u16(status).unwrap());

    match future
        .and_then(move |_| async move {
            valid_fisher_session(conn, &create_wish_req.session_token).await
        })
        .and_then(move |fisher_id| async move {
            event_exists_for_fisher(conn, create_wish_req.event_id, fisher_id)
                .map_err(|e| easy_fatal_error("checking event exists", e))
                .await
        })
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                add_new_wish(conn, create_wish_req)
                    .map_err(|e| easy_fatal_error("adding new wish", e))
                    .await
            }
        })
        .await
    {
        Ok(v) => Ok(CreateWishResponse { wish_id: v }),
        Err(e) => Err(e),
    }
}

pub async fn update_wish(
    conn: &DbConn,
    update_wish_req: &UpdateWishRequest,
) -> Result<StatusCode, StatusCode> {
    let future = valid_wish(SimpleWish::from(update_wish_req))
        .map_err(|status| StatusCode::from_u16(status).unwrap());

    match future
        .and_then(move |_| async move {
            valid_fisher_session(conn, &update_wish_req.session_token).await
        })
        .and_then(move |fisher_id| async move {
            event_exists_for_fisher(conn, update_wish_req.event_id, fisher_id)
                .map_err(|e| easy_fatal_error("checking if event exists", e))
                .await
        })
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                wish_exists(conn, update_wish_req.wish_id, update_wish_req.event_id)
                    .map_err(|e| easy_fatal_error("checking if wish exists", e))
                    .await
            }
        })
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                update_wish_details(conn, update_wish_req)
                    .map_err(|e| easy_fatal_error("updating wish", e))
                    .await
            }
        })
        .await
    {
        Ok(_) => Ok(StatusCode::from_u16(204).unwrap()),
        Err(e) => Err(e),
    }
}

pub async fn delete_wish(
    conn: &DbConn,
    delete_wish_req: &DeleteWishRequest,
) -> Result<StatusCode, StatusCode> {
    let future = valid_fisher_session(conn, &delete_wish_req.session_token);

    match future
        .and_then(move |fisher_id| async move {
            event_exists_for_fisher(conn, delete_wish_req.event_id, fisher_id)
                .map_err(|e| easy_fatal_error("checking if event exists", e))
                .await
        })
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                delete_wish_data(conn, delete_wish_req)
                    .map_err(|e| easy_fatal_error("deleting wish", e))
                    .await
            }
        })
        .await
    {
        Ok(()) => Ok(StatusCode::from_u16(204).unwrap()),
        Err(e) => Err(e),
    }
}

pub async fn get_wishes(
    conn: &DbConn,
    get_wishes_req: &GetWishesRequest,
) -> Result<Vec<GetWishesResponse>, StatusCode> {
    let future = valid_fisher_session(conn, &get_wishes_req.session_token);

    match future
        .and_then(move |fisher_id| async move {
            event_exists_for_fisher(conn, get_wishes_req.event_id, fisher_id)
                .map_err(|e| easy_fatal_error("checking if event exists", e))
                .await
        })
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                get_wishes_for_event(conn, get_wishes_req.event_id)
                    .map_err(|e| easy_fatal_error("getting wishes for event", e))
                    .await
            }
        })
        .await
    {
        Ok(v) => Ok(v),
        Err(e) => Err(e),
    }
}

pub async fn claim_wish(
    conn: &DbConn,
    claim_wish_req: &ClaimWishRequest,
) -> Result<StatusCode, StatusCode> {
    let future = valid_fish_session(conn, &claim_wish_req.session_token);

    match future
        .and_then(move |fish_id| async move {
            event_exists_for_fish(conn, fish_id, claim_wish_req.event_id)
                .map_ok(|exists| (exists, fish_id))
                .map_err(|e| easy_fatal_error("checking event exists", e))
                .await
        })
        .and_then(move |(exists, fish_id)| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                check_wish_can_be_claimed(conn, claim_wish_req.wish_id, claim_wish_req.event_id)
                    .map_ok(|claimable| (claimable, fish_id))
                    .map_err(|e| easy_fatal_error("checking if fish can claim event", e))
                    .await
            }
        })
        .and_then(move |(claimable, fish_id)| async move {
            if claimable {
                set_wish_claimed(conn, claim_wish_req.wish_id, fish_id)
                    .map_err(|e| easy_fatal_error("claiming wish", e))
                    .await
            } else {
                Err(StatusCode::from_u16(403).unwrap())
            }
        })
        .await
    {
        Ok(()) => Ok(StatusCode::from_u16(204).unwrap()),
        Err(e) => Err(e),
    }
}
