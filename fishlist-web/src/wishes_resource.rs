use actix_web::{
    delete, get, post, put,
    web::{Data, Json},
    HttpResponse, Responder,
};
use fishlist_base::wishes::{
    ClaimWishRequest, CreateWishRequest, DeleteWishRequest, GetWishesRequest, UpdateWishRequest,
};
use fishlist_data::types::DbConn;

mod internal;

#[post("/create/event/wish")]
pub async fn add_wish(conn: Data<DbConn>, new_wish: Json<CreateWishRequest>) -> impl Responder {
    match internal::add_wish(&conn, &new_wish).await {
        Ok(v) => HttpResponse::Ok().json(v),
        Err(e) => HttpResponse::new(e),
    }
}

#[put("/edit/event/wish")]
pub async fn update_wish(
    conn: Data<DbConn>,
    update_req: Json<UpdateWishRequest>,
) -> impl Responder {
    HttpResponse::new(match internal::update_wish(&conn, &update_req).await {
        Ok(v) => v,
        Err(e) => e,
    })
}

#[delete("/delete/event/wish")]
pub async fn delete_wish(
    conn: Data<DbConn>,
    delete_req: Json<DeleteWishRequest>,
) -> impl Responder {
    HttpResponse::new(match internal::delete_wish(&conn, &delete_req).await {
        Ok(v) => v,
        Err(e) => e,
    })
}

#[get("/auth/get/wishes")]
pub async fn get_wishes(
    conn: Data<DbConn>,
    get_wishes_req: Json<GetWishesRequest>,
) -> impl Responder {
    match internal::get_wishes(&conn, &get_wishes_req).await {
        Ok(v) => HttpResponse::Ok().json(v),
        Err(e) => HttpResponse::new(e),
    }
}

#[put("/fish/wish/claim")]
pub async fn claim_wish(
    conn: Data<DbConn>,
    claim_wish_req: Json<ClaimWishRequest>,
) -> impl Responder {
    HttpResponse::new(match internal::claim_wish(&conn, &claim_wish_req).await {
        Ok(v) => v,
        Err(e) => e,
    })
}
