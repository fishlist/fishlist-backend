use actix_web::http::StatusCode;
use fishlist_data::{
    fish_common::{self, FishInvalidSessionError},
    fisher_common::{self, FisherInvalidSessionError},
    types::{DbConn, SqlError},
};
use futures_util::future::TryFutureExt;
use log::error;

pub fn easy_fatal_error(log_prefix: &str, e: SqlError) -> StatusCode {
    error!("Error {}: {}", log_prefix, e);
    StatusCode::from_u16(500).unwrap()
}

/// Checks for valid session and automatically maps to proper actix error types.
/// Returns fisher_id if session is valid.
pub async fn valid_fisher_session(conn: &DbConn, session_token: &str) -> Result<i64, StatusCode> {
    fisher_common::valid_session(conn, session_token)
        .map_err(|e| match e {
            FisherInvalidSessionError::TokenExpired => StatusCode::from_u16(401).unwrap(),
            FisherInvalidSessionError::TokenDoesNotExist => StatusCode::from_u16(403).unwrap(),
            FisherInvalidSessionError::SqlError(sql_e) => {
                easy_fatal_error("checking session data", sql_e)
            }
        })
        .await
}

/// Checks for valid session and automatically maps to proper actix error types.
/// Returns fisher_id if session is valid.
pub async fn valid_fish_session(conn: &DbConn, session_token: &str) -> Result<i64, StatusCode> {
    fish_common::valid_session(conn, session_token)
        .map_err(|e| match e {
            FishInvalidSessionError::TokenExpired => StatusCode::from_u16(401).unwrap(),
            FishInvalidSessionError::TokenDoesNotExist => StatusCode::from_u16(403).unwrap(),
            FishInvalidSessionError::SqlError(sql_e) => {
                easy_fatal_error("checking session data", sql_e)
            }
        })
        .await
}
