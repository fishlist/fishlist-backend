use actix_web::http::StatusCode;
use fishlist_base::fish_events::{AddFishToEventRequest, FishAddedToEventResponse};

use fishlist_data::{
    events::event_exists_for_fisher, fish_events::add_fish_to_event_db, types::DbConn,
};
use futures_util::future::TryFutureExt;
use util::{easy_fatal_error, valid_fisher_session};

use crate::util;

pub async fn add_fish_to_event(
    conn: &DbConn,
    add_to_event_req: &AddFishToEventRequest,
) -> Result<Vec<FishAddedToEventResponse>, StatusCode> {
    let future = valid_fisher_session(conn, &add_to_event_req.session_token);

    future
        .and_then(move |fisher_id| async move {
            event_exists_for_fisher(conn, add_to_event_req.event_id, fisher_id)
                .await
                .map_err(|e| easy_fatal_error("checking if event exists", e))
        })
        .and_then(move |exists| async move {
            if !exists {
                Err(StatusCode::from_u16(404).unwrap())
            } else {
                add_fish_to_event_db(conn, add_to_event_req)
                    .map_err(|e| easy_fatal_error("adding fish(es) to event", e))
                    .await
            }
        })
        .await
}
