all: dev.sqlite3
	cargo build

release:
	cargo build --release
	strip target/release/fishlist-backend

dev.sqlite3:
	cat setup/up.sql | sqlite3 dev.sqlite3

check:
	cargo clippy -- -D warnings

clean: clean-target clean-db

clean-target:
	cargo clean

clean-db:
	rm -f dev.sqlite3*

