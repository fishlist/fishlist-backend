use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct AddFishToEventRequest {
    pub fish_emails: Vec<String>,
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Deserialize, Serialize)]
pub struct FishAddedToEventResponse {
    pub fish_email: String,
    pub fish_id: i64,
}
