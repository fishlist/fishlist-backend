use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct CreateEventRequest {
    pub event_name: String,
    pub event_time: i64,
    pub private: bool,
    pub session_token: String,
}

#[derive(Deserialize, Serialize)]
pub struct CreateEventResponse {
    pub event_id: i64,
}

#[derive(Deserialize, Serialize)]
pub struct UpdateEventRequest {
    pub event_name: String,
    pub event_time: i64,
    pub private: bool,
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Deserialize, Serialize)]
pub struct DeleteEventRequest {
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Deserialize, Serialize)]
pub struct GetEventsRequest {
    pub session_token: String,
}

#[derive(Deserialize, Serialize)]
pub struct GetEventsResponse {
    pub event_name: String,
    pub event_time: i64,
    pub private: bool,
    pub event_id: i64,
}
