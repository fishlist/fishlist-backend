use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct RegisterRequest {
    pub email: String,
    pub display_name: String,
    pub first_name: String,
    pub last_name: String,
    pub password: String,
}

#[derive(Deserialize, Serialize)]
pub struct LoginRequest {
    pub email: String,
    pub password: String,
}

#[derive(Deserialize, Serialize)]
pub struct LoginResponse {
    pub token: String,
}

#[derive(Deserialize, Serialize)]
pub struct LogoutRequest {
    pub token: String,
}
