use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct CreateWishRequest {
    pub wish_title: String,
    pub wish_link: String,
    pub wish_description: String,
    pub multi_claim: bool,
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Serialize, Deserialize)]
pub struct CreateWishResponse {
    pub wish_id: i64,
}

#[derive(Serialize, Deserialize)]
pub struct UpdateWishRequest {
    pub wish_title: String,
    pub wish_link: String,
    pub wish_description: String,
    pub multi_claim: bool,
    pub wish_id: i64,
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeleteWishRequest {
    pub wish_id: i64,
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Serialize, Deserialize)]
pub struct GetWishesRequest {
    pub event_id: i64,
    pub session_token: String,
}

#[derive(Serialize, Deserialize)]
pub struct GetWishesResponse {
    pub wish_title: String,
    pub wish_link: String,
    pub wish_description: String,
    pub multi_claim: bool,
    pub updated_at: i64,
    pub wish_id: i64,
    pub event_id: i64,
}

#[derive(Serialize, Deserialize)]
pub struct ClaimWishRequest {
    pub session_token: String,
    pub wish_id: i64,
    pub event_id: i64,
}
