use sqlx::Error;
use std::time::{SystemTime, UNIX_EPOCH};
use types::DbConn;

use crate::types;

#[derive(sqlx::FromRow)]
struct SimpleSessionData {
    fisher_id: i64,
    expiration: i64,
}

pub enum FisherInvalidSessionError {
    TokenExpired,
    TokenDoesNotExist,
    SqlError(Error),
}

/// Checks to see if session is valid and returns fisher_id if it is.
pub async fn valid_session(
    conn: &DbConn,
    session_token: &str,
) -> Result<i64, FisherInvalidSessionError> {
    let session_data = sqlx::query_as::<_, SimpleSessionData>(
        "SELECT fisher_id, expiration FROM fisher_sessions WHERE session_token = ?",
    )
    .bind(session_token)
    .fetch_optional(conn)
    .await
    .map_err(FisherInvalidSessionError::SqlError)?;

    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");

    match session_data {
        Some(s) => {
            if s.expiration > since_the_epoch.as_millis() as i64 {
                Ok(s.fisher_id)
            } else {
                Err(FisherInvalidSessionError::TokenExpired)
            }
        }
        None => Err(FisherInvalidSessionError::TokenDoesNotExist),
    }
}

pub async fn get_email_from_session_id(conn: &DbConn, session_id: &str) -> Result<String, Error> {
    let fisher_id = get_id_from_session_token(conn, session_id).await?;

    let email: (String,) = sqlx::query_as("SELECT fisher_email FROM fishers WHERE fisher_id = ?")
        .bind(fisher_id)
        .fetch_one(conn)
        .await?;

    Ok(email.0)
}

pub async fn get_id_from_session_token(conn: &DbConn, session_token: &str) -> Result<i64, Error> {
    let fisher_id: (i64,) =
        sqlx::query_as("SELECT fisher_id FROM fisher_sessions WHERE session_token = ?")
            .bind(session_token)
            .fetch_one(conn)
            .await?;

    Ok(fisher_id.0)
}

pub async fn get_id_from_email(conn: &DbConn, email: &str) -> Result<i64, Error> {
    let result: (i64,) = sqlx::query_as("SELECT fisher_id FROM fishers WHERE fisher_email = ?")
        .bind(email)
        .fetch_one(conn)
        .await?;
    Ok(result.0)
}

pub async fn fisher_exists(
    conn: &DbConn,
    user_email: &str,
    user_display_name: &str,
) -> Result<bool, Error> {
    Ok(fisher_email_exists(conn, user_email).await?
        || fisher_display_exists(conn, user_display_name).await?)
}

pub async fn fisher_email_exists(conn: &DbConn, user_email: &str) -> Result<bool, Error> {
    let num_emails: (i64,) = sqlx::query_as("SELECT COUNT(1) FROM fishers WHERE fisher_email = ?")
        .bind(user_email)
        .fetch_one(conn)
        .await?;

    Ok(num_emails.0 > 0)
}

pub async fn fisher_display_exists(conn: &DbConn, user_display_name: &str) -> Result<bool, Error> {
    let num_display_names: (i64,) =
        sqlx::query_as("SELECT COUNT(1) FROM fishers WHERE display_name = ?")
            .bind(user_display_name)
            .fetch_one(conn)
            .await?;

    Ok(num_display_names.0 > 0)
}
