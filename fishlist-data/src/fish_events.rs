use fishlist_base::fish_events::{AddFishToEventRequest, FishAddedToEventResponse};
use sqlx::{Any, Error, Executor, Transaction};

use types::DbConn;

use crate::types;

pub async fn add_fish_to_event_db(
    conn: &DbConn,
    add_req: &AddFishToEventRequest,
) -> Result<Vec<FishAddedToEventResponse>, Error> {
    let mut tx = conn.begin().await?;
    let added_fish = internal_add_fish_to_event(&mut tx, add_req).await?;
    tx.commit().await?;
    Ok(added_fish)
}

async fn internal_add_fish_to_event<'a>(
    tx: &mut Transaction<'a, Any>,
    add_req: &'a AddFishToEventRequest,
) -> Result<Vec<FishAddedToEventResponse>, Error> {
    let mut fishes: Vec<FishAddedToEventResponse> = vec![];
    for fish_email in add_req.fish_emails.iter() {
        let fish_id = tx
            .execute(
                sqlx::query("INSERT INTO fishes (event_id, fish_email) VALUES(?, ?)")
                    .bind(add_req.event_id)
                    .bind(&fish_email),
            )
            .await?
            .last_insert_id();

        match fish_id {
            Some(v) => fishes.push(FishAddedToEventResponse {
                fish_email: fish_email.to_string(),
                fish_id: v,
            }),
            None => return Err(sqlx::Error::RowNotFound),
        };
    }
    Ok(vec![])
}
