use dotenv::dotenv;
use log::error;
use sqlx::{any::AnyPoolOptions, AnyPool};
use std::{env, io, io::Error};

pub mod events;
pub mod fish_events;
pub mod fish_common;
pub mod fisher_auth;
pub mod fisher_common;
pub mod types;
pub mod wishes;

pub async fn init_data_pool() -> Result<AnyPool, io::Error> {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    Ok(AnyPoolOptions::new()
        .max_connections(10)
        .connect(&database_url)
        .await
        .map_err(|e| {
            error!("Encountered error trying to make database pool: {}", e);
            Error::new(io::ErrorKind::ConnectionAborted, e)
        })?)
}
