use sqlx::{any::AnyPool, error::Error};

pub type DbConn = AnyPool;
pub type SqlError = Error;
