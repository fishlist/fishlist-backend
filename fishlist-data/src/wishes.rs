use fishlist_base::wishes::{
    CreateWishRequest, DeleteWishRequest, GetWishesResponse, UpdateWishRequest,
};
use sqlx::{any::AnyRow, Error, Executor, Row};
use std::time::{SystemTime, UNIX_EPOCH};
use types::DbConn;

use crate::types;

pub async fn get_wishes_for_event(
    conn: &DbConn,
    event_id: i64,
) -> Result<Vec<GetWishesResponse>, Error> {
    Ok(sqlx::query(
        "SELECT
            wish_title,
            wish_link,
            wish_description,
            multi_claim,
            updated_at,
            wish_id,
            event_id
        FROM wishes WHERE event_id = ?",
    )
    .bind(event_id)
    .map(|row: AnyRow| GetWishesResponse {
        wish_title: row.get(0),
        wish_link: row.get(1),
        wish_description: row.get(2),
        multi_claim: row.get(3),
        updated_at: row.get(4),
        wish_id: row.get(5),
        event_id: row.get(6),
    })
    .fetch_all(conn)
    .await?)
}

pub async fn add_new_wish(conn: &DbConn, new_wish: &CreateWishRequest) -> Result<i64, Error> {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");

    let wish_id = conn
        .execute(
            sqlx::query(
                r#"INSERT INTO wishes
                    (event_id, wish_title, wish_link, wish_description, multi_claim, order_number, updated_at)
                    VALUES(?, ?, ?, ?, ?, ?, ?)"#,
            )
            .bind(new_wish.event_id)
            .bind(&new_wish.wish_title)
            .bind(&new_wish.wish_description)
            .bind(&new_wish.wish_link)
            .bind(new_wish.multi_claim)
            .bind(0_i64) // all new events are put at the very front of the list
            .bind(since_the_epoch.as_millis() as i64),
        )
        .await?
        .last_insert_id();

    Ok(wish_id.unwrap())
}

pub async fn update_wish_details(
    conn: &DbConn,
    update_wish: &UpdateWishRequest,
) -> Result<(), Error> {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");

    sqlx::query(
        r#"UPDATE wishes
        SET wish_title = ?, wish_link = ?, wish_description = ?, multi_claim = ?, updated_at = ?
        WHERE wish_id = ? AND event_id = ?"#,
    )
    .bind(&update_wish.wish_title)
    .bind(&update_wish.wish_link)
    .bind(&update_wish.wish_description)
    .bind(update_wish.multi_claim)
    .bind(since_the_epoch.as_millis() as i64)
    .bind(update_wish.wish_id)
    .bind(update_wish.event_id)
    .execute(conn)
    .await?;

    Ok(())
}

pub async fn wish_exists(conn: &DbConn, wish_id: i64, event_id: i64) -> Result<bool, Error> {
    let count: (i64,) =
        sqlx::query_as("SELECT COUNT(1) FROM wishes WHERE wish_id = ? and event_id = ?")
            .bind(wish_id)
            .bind(event_id)
            .fetch_one(conn)
            .await?;

    Ok(count.0 > 0)
}

pub async fn delete_wish_data(
    conn: &DbConn,
    delete_request: &DeleteWishRequest,
) -> Result<(), Error> {
    sqlx::query("DELETE FROM wishes WHERE wish_id = ? AND event_id = ?")
        .bind(delete_request.wish_id)
        .bind(delete_request.event_id)
        .execute(conn)
        .await?;

    Ok(())
}

pub async fn check_wish_can_be_claimed(
    conn: &DbConn,
    wish_id: i64,
    event_id: i64,
) -> Result<bool, Error> {
    let claims: (i64,) = sqlx::query_as(
        "SELECT COUNT(1) FROM wish_claim_info WHERE wish_id = ? AND event_id = ? LIMIT 1",
    )
    .bind(wish_id)
    .bind(event_id)
    .fetch_one(conn)
    .await?;

    if claims.0 > 0 {
        Ok(check_wish_multi_claim(conn, wish_id, event_id).await?)
    } else {
        Ok(true)
    }
}

pub async fn set_wish_claimed(conn: &DbConn, wish_id: i64, fish_id: i64) -> Result<(), Error> {
    sqlx::query("INSERT INTO wish_claim_info (wish_id, fish_id) VALUES (?, ?)")
        .bind(wish_id)
        .bind(fish_id)
        .execute(conn)
        .await?;
    Ok(())
}

async fn check_wish_multi_claim(conn: &DbConn, wish_id: i64, event_id: i64) -> Result<bool, Error> {
    let multi_claim: (bool,) =
        sqlx::query_as("SELECT multi_claim FROM wishes WHERE wish_id = ? AND event_id = ?")
            .bind(wish_id)
            .bind(event_id)
            .fetch_one(conn)
            .await?;

    Ok(multi_claim.0)
}
