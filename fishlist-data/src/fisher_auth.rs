use fishlist_base::fisher_auth::{LoginRequest, LoginResponse, RegisterRequest};
use fishlist_core::sec::{get_token_expiration, hash_pw};
use log::error;
use sqlx::{Any, Error, Executor, Transaction};
use types::DbConn;
use uuid::Uuid;

use crate::types;

pub async fn setup_new_session(conn: &DbConn, fisher_id: i64) -> Result<LoginResponse, Error> {
    let token = Uuid::new_v4().to_string();
    conn.execute(
        sqlx::query("INSERT INTO fisher_sessions (fisher_id, session_token, expiration, reset_token) VALUES (?, ?, ?, ?)")
            .bind(fisher_id)
            .bind(&token)
            .bind(get_token_expiration())
            .bind(0_i64)
    )
    .await?;

    Ok(LoginResponse { token })
}

pub async fn add_new_fisher(
    conn: &DbConn,
    register_request: &RegisterRequest,
) -> Result<(), Error> {
    let mut tx = conn.begin().await?;
    internal_add_new_fisher(&mut tx, register_request).await?;
    tx.commit().await?;
    Ok(())
}

pub async fn get_hashword(conn: &DbConn, login_request: &LoginRequest) -> Result<String, Error> {
    let hashword: (String,) = sqlx::query_as("SELECT hashword FROM fishers WHERE fisher_email = ?")
        .bind(&login_request.email)
        .fetch_one(conn)
        .await?;

    Ok(hashword.0)
}

async fn internal_add_new_fisher<'a>(
    tx: &mut Transaction<'a, Any>,
    register_request: &'a RegisterRequest,
) -> Result<(), Error> {
    let hashword = &hash_pw(&register_request.password).map_err(|e| {
        error!("Failed to hash password: {}", e);
        Error::WorkerCrashed
    })?;
    let fisher_id = tx
        .execute(
            sqlx::query(
                "INSERT INTO fishers (fisher_email, display_name, hashword) VALUES(?, ?, ?)",
            )
            .bind(&register_request.email)
            .bind(&register_request.display_name)
            .bind(&hashword),
        )
        .await?
        .last_insert_id();

    if fisher_id.is_none() {
        return Err(sqlx::Error::ColumnNotFound(
            "Could not find last insert id during register process".to_owned(),
        ));
    }

    tx.execute(
        sqlx::query(
            "INSERT INTO fisher_info (
                fisher_id,
                first_name,
                last_name
            )
            VALUES (?, ?, ?)",
        )
        .bind(fisher_id.unwrap())
        .bind(&register_request.first_name)
        .bind(&register_request.last_name),
    )
    .await?;

    Ok(())
}

pub async fn delete_session_token(conn: &DbConn, token: &str) -> Result<(), Error> {
    sqlx::query("DELETE FROM fisher_sessions WHERE session_token = ?")
        .bind(token)
        .execute(conn)
        .await?;

    Ok(())
}
