use fishlist_base::events::{
    CreateEventRequest, DeleteEventRequest, GetEventsResponse, UpdateEventRequest,
};
use sqlx::{
    any::AnyRow,
    Row, {Error, Executor},
};
use types::DbConn;

use crate::types;

pub async fn create_new_event(
    conn: &DbConn,
    new_event: &CreateEventRequest,
    fisher_id: i64,
) -> Result<i64, Error> {
    let event_id = conn
        .execute(
            sqlx::query(
                "INSERT INTO events (fisher_id, event_name, event_time, private) VALUES(?, ?, ?, ?)",
            )
            .bind(fisher_id)
            .bind(&new_event.event_name)
            .bind(new_event.event_time)
            .bind(new_event.private),
        )
        .await?
        .last_insert_id();

    if event_id.is_none() {
        return Err(Error::ColumnNotFound(
            "Could not find last insert id during create event process".to_owned(),
        ));
    }

    Ok(event_id.unwrap())
}

pub async fn update_event(
    conn: &DbConn,
    update_event: &UpdateEventRequest,
    fisher_id: i64,
) -> Result<(), Error> {
    sqlx::query(
        "UPDATE events SET event_name = ?, event_time = ?, private = ? WHERE event_id = ? AND fisher_id = ?",
    )
    .bind(&update_event.event_name)
    .bind(update_event.event_time)
    .bind(update_event.private)
    .bind(update_event.event_id)
    .bind(fisher_id)
    .execute(conn)
    .await?;

    Ok(())
}

pub async fn delete_event(
    conn: &DbConn,
    delete_event: &DeleteEventRequest,
    fisher_id: i64,
) -> Result<(), Error> {
    sqlx::query("DELETE FROM events WHERE event_id = ? AND fisher_id = ?")
        .bind(&delete_event.event_id)
        .bind(fisher_id)
        .execute(conn)
        .await?;

    Ok(())
}

pub async fn event_exists_for_fish(
    conn: &DbConn,
    fish_id: i64,
    event_id: i64,
) -> Result<bool, Error> {
    let fish: (i64,) =
        sqlx::query_as("SELECT COUNT(1) FROM fishes WHERE fish_id = ? AND event_id = ?")
            .bind(fish_id)
            .bind(event_id)
            .fetch_one(conn)
            .await?;

    Ok(fish.0 > 0)
}

pub async fn event_exists_for_fisher(
    conn: &DbConn,
    event_id: i64,
    fisher_id: i64,
) -> Result<bool, Error> {
    let event: (i64,) =
        sqlx::query_as("SELECT COUNT(1) FROM events WHERE event_id = ? AND fisher_id = ?")
            .bind(event_id)
            .bind(fisher_id)
            .fetch_one(conn)
            .await?;

    Ok(event.0 > 0)
}

pub async fn get_event_data(
    conn: &DbConn,
    fisher_id: i64,
) -> Result<Vec<GetEventsResponse>, Error> {
    Ok(sqlx::query(
        "SELECT event_name, event_time, event_id, private FROM events WHERE fisher_id = ? ORDER BY event_time DESC",
    )
    .bind(fisher_id)
    .map(|row: AnyRow| GetEventsResponse {
        event_name: row.get(0),
        event_time: row.get(1),
        event_id: row.get(2),
        private: row.get(3)
    })
    .fetch_all(conn)
    .await?)
}
