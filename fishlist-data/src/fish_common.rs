use sqlx::Error;
use std::time::{SystemTime, UNIX_EPOCH};
use types::DbConn;

use crate::types;

#[derive(sqlx::FromRow)]
struct SimpleSessionData {
    fish_id: i64,
    expiration: i64,
}

pub enum FishInvalidSessionError {
    TokenExpired,
    TokenDoesNotExist,
    SqlError(Error),
}

pub async fn valid_session(
    conn: &DbConn,
    session_token: &str,
) -> Result<i64, FishInvalidSessionError> {
    let session_data = sqlx::query_as::<_, SimpleSessionData>(
        "SELECT fish_id, expiration FROM fish_sessions WHERE session_token = ?",
    )
    .bind(session_token)
    .fetch_optional(conn)
    .await
    .map_err(FishInvalidSessionError::SqlError)?;

    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");

    match session_data {
        Some(s) => {
            if s.expiration > since_the_epoch.as_millis() as i64 {
                Ok(s.fish_id)
            } else {
                Err(FishInvalidSessionError::TokenExpired)
            }
        }
        None => Err(FishInvalidSessionError::TokenDoesNotExist),
    }
}
