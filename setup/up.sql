CREATE TABLE IF NOT EXISTS fishers(
  fisher_id INTEGER NOT NULL PRIMARY KEY,
  fisher_email TEXT NOT NULL,
  display_name TEXT NOT NULL,
  hashword TEXT NOT NULL
);
CREATE UNIQUE INDEX fisher_uniq_idx ON FISHERS (fisher_email);
CREATE UNIQUE INDEX display_name_uniq_idx ON FISHERS (display_name);
CREATE TABLE IF NOT EXISTS fisher_info(
  fisher_info_id INTEGER NOT NULL PRIMARY KEY,
  fisher_id INTEGER NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  FOREIGN KEY (fisher_id) REFERENCES fishers(fisher_id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fisher_id_uniq_idx ON fisher_info (fisher_id);
CREATE TABLE IF NOT EXISTS events(
  event_id INTEGER NOT NULL PRIMARY KEY,
  fisher_id INTEGER NOT NULL,
  event_name TEXT NOT NULL,
  event_time INTEGER NOT NULL,
  private TINYINT(1) NOT NULL,
  FOREIGN KEY (fisher_id) REFERENCES fishers(fisher_id) ON DELETE CASCADE
);
CREATE INDEX events_fisher_id_idx ON events (fisher_id);
CREATE TABLE IF NOT EXISTS wishes(
  wish_id INTEGER NOT NULL PRIMARY KEY,
  event_id INTEGER NOT NULL,
  wish_title TEXT NOT NULL,
  wish_link TEXT NOT NULL,
  wish_description TEXT NOT NULL,
  multi_claim TINYINT(1) NOT NULL,
  order_number INT NOT NULL,
  updated_at INTEGER NOT NULL,
  FOREIGN KEY(event_id) REFERENCES events(event_id) ON DELETE CASCADE
);
CREATE INDEX event_id_idx ON wishes (event_id);
CREATE TABLE IF NOT EXISTS fishes(
  fish_id INTEGER NOT NULL PRIMARY KEY,
  event_id INTEGER NOT NULL,
  fish_email TEXT NOT NULL,
  FOREIGN KEY (event_id) REFERENCES events(event_id) ON DELETE CASCADE,
);
CREATE INDEX fish_email_uniq_idx ON fishes (fish_email);
CREATE TABLE IF NOT EXISTS wish_claim_info(
  wish_info_id INTEGER NOT NULL PRIMARY KEY,
  wish_id INTEGER NOT NULL,
  claimed_by_fish INTEGER NOT NULL,
  FOREIGN KEY (wish_id) REFERENCES wishes(wish_id) ON DELETE CASCADE,
  FOREIGN KEY (claimed_by_fish) REFERENCES fishes(fish_id) ON DELETE CASCADE
);
CREATE INDEX wish_id_idx ON wish_claim_info (wish_id);
CREATE TABLE IF NOT EXISTS fisher_sessions(
  session_id INTEGER NOT NULL PRIMARY KEY,
  fisher_id INTEGER NOT NULL,
  session_token CHAR(36) NOT NULL,
  expiration BIGINT NOT NULL,
  reset_token TINYINT NOT NULL,
  FOREIGN KEY (fisher_id) REFERENCES fishers(fisher_id) ON DELETE CASCADE
);
CREATE INDEX fisher_sessions_fisher_id_idx ON fisher_sessions (fisher_id);
CREATE TABLE IF NOT EXISTS fish_sessions(
  session_id INTEGER NOT NULL PRIMARY KEY,
  fish_id INTEGER NOT NULL,
  session_token CHAR(36) NOT NULL,
  expiration BIGINT NOT NULL,
  FOREIGN KEY (fish_id) REFERENCES fishes(fish_id) ON DELETE CASCADE
);
CREATE INDEX fish_sessions_fisher_id_idx ON fish_sessions (fish_id);
