use fishlist_base::events::{CreateEventRequest, UpdateEventRequest};
use std::{
    convert::From,
    time::{SystemTime, UNIX_EPOCH},
};

static MAX_NAME_LEN: usize = 256;

pub struct SimpleEvent {
    event_name: String,
    event_time: i64,
}

impl From<&CreateEventRequest> for SimpleEvent {
    fn from(event: &CreateEventRequest) -> Self {
        SimpleEvent {
            event_name: event.event_name.clone(),
            event_time: event.event_time,
        }
    }
}

impl From<&UpdateEventRequest> for SimpleEvent {
    fn from(event: &UpdateEventRequest) -> Self {
        SimpleEvent {
            event_name: event.event_name.clone(),
            event_time: event.event_time,
        }
    }
}

pub async fn event_valid(simple_event: SimpleEvent) -> Result<(), u16> {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    if simple_event.event_time < since_the_epoch.as_millis() as i64
        || simple_event.event_name.chars().count() > MAX_NAME_LEN
    {
        Err(400)
    } else {
        Ok(())
    }
}
