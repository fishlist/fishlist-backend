use fishlist_base::wishes::{CreateWishRequest, UpdateWishRequest};
use std::convert::From;
use url::Url;

static MAX_TITLE_LEN: usize = 256;
static MAX_LINK_LEN: usize = 2048;
static MAX_DESCRIPTION_LEN: usize = 1024;

pub struct SimpleWish {
    wish_title: String,
    wish_link: String,
    wish_description: String,
}

impl From<&CreateWishRequest> for SimpleWish {
    fn from(create_req: &CreateWishRequest) -> Self {
        SimpleWish {
            wish_title: create_req.wish_title.clone(),
            wish_link: create_req.wish_link.clone(),
            wish_description: create_req.wish_description.clone(),
        }
    }
}

impl From<&UpdateWishRequest> for SimpleWish {
    fn from(update_req: &UpdateWishRequest) -> Self {
        SimpleWish {
            wish_title: update_req.wish_title.clone(),
            wish_link: update_req.wish_link.clone(),
            wish_description: update_req.wish_description.clone(),
        }
    }
}

pub async fn valid_wish(create_req: SimpleWish) -> Result<(), u16> {
    if create_req.wish_title.chars().count() < MAX_TITLE_LEN
        && create_req.wish_description.chars().count() < MAX_DESCRIPTION_LEN
        && create_req.wish_link.chars().count() < MAX_LINK_LEN
        && Url::parse(&create_req.wish_link).is_ok()
    {
        Ok(())
    } else {
        Err(400)
    }
}
