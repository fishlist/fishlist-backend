use bcrypt::{hash, verify, BcryptResult, DEFAULT_COST};
use std::time::{Duration, SystemTime, UNIX_EPOCH};

static _ONE_DAY: u64 = 86_400;
static _ONE_MONTH: u64 = _ONE_DAY * 30;
static ONE_MONTH: Duration = Duration::from_secs(_ONE_MONTH);

pub fn verify_pw(password: &str, hashed_password: &str) -> BcryptResult<bool> {
    verify(password, hashed_password)
}

pub fn hash_pw(password: &str) -> BcryptResult<String> {
    hash(password, DEFAULT_COST)
}

pub fn get_token_expiration() -> i64 {
    let epoch_millis = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("time went backwards");

    (epoch_millis.as_millis() + ONE_MONTH.as_millis()) as i64
}
